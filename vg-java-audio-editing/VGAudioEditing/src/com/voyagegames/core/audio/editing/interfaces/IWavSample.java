package com.voyagegames.core.audio.editing.interfaces;

public interface IWavSample {

	short bitSize();
	short numChannels();
	short format();
	int sampleRate();
	int numSamples();
	int durationInMilliSeconds();
	double toneFrequencyHz();
	int maximumAmplitude();
	
	double[] sample();
	IWavSample setSample(double[] sample);
	IWavSample generateSample();
	
	byte[] generated();
	IWavSample setGenerated(byte[] generated);
	IWavSample finalizeOutput();
	void generateSampleFromGenerated();

}
