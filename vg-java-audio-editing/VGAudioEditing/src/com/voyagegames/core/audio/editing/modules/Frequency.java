package com.voyagegames.core.audio.editing.modules;

// From: http://en.wikipedia.org/wiki/Piano_key_frequencies
// Frequencies reported in Hz, based on piano tuning
public class Frequency {

	//
	// Standard notes
	//
	
	public static final double C_8      = 4186.01;
	public static final double B_7      = 3951.07;
	public static final double B_7_FLAT = 3729.31;
	public static final double A_7      = 3520.00;
	public static final double A_7_FLAT = 3322.44;
	public static final double G_7      = 3135.96;
	public static final double G_7_FLAT = 2959.96;
	public static final double F_7      = 2793.83;
	public static final double E_7      = 2637.02;
	public static final double E_7_FLAT = 2489.02;
	public static final double D_7      = 2349.32;
	public static final double D_7_FLAT = 2217.46;
	public static final double C_7      = 2093.00;
	public static final double B_6      = 1975.53;
	public static final double B_6_FLAT = 1864.66;
	public static final double A_6      = 1760.00;
	public static final double A_6_FLAT = 1661.22;
	public static final double G_6      = 1567.98;
	public static final double G_6_FLAT = 1479.98;
	public static final double F_6      = 1396.91;
	public static final double E_6      = 1318.51;
	public static final double E_6_FLAT = 1244.51;
	public static final double D_6      = 1174.66;
	public static final double D_6_FLAT = 1108.73;
	public static final double C_6      = 1046.50;
	public static final double B_5      = 987.767;
	public static final double B_5_FLAT = 932.328;
	public static final double A_5      = 880.000;
	public static final double A_5_FLAT = 830.609;
	public static final double G_5      = 783.991;
	public static final double G_5_FLAT = 739.989;
	public static final double F_5      = 698.456;
	public static final double E_5      = 659.255;
	public static final double E_5_FLAT = 622.254;
	public static final double D_5      = 587.330;
	public static final double D_5_FLAT = 554.365;
	public static final double C_5      = 523.251;
	public static final double B_4      = 493.883;
	public static final double B_4_FLAT = 466.164;
	public static final double A_4      = 440.000;
	public static final double A_4_FLAT = 415.305;
	public static final double G_4      = 391.995;
	public static final double G_4_FLAT = 369.994;
	public static final double F_4      = 349.228;
	public static final double E_4      = 329.628;
	public static final double E_4_FLAT = 311.127;
	public static final double D_4      = 293.665;
	public static final double D_4_FLAT = 277.183;
	public static final double C_4      = 261.626;
	public static final double B_3      = 246.942;
	public static final double B_3_FLAT = 233.082;
	public static final double A_3      = 220.000;
	public static final double A_3_FLAT = 207.652;
	public static final double G_3      = 195.998;
	public static final double G_3_FLAT = 184.997;
	public static final double F_3      = 174.614;
	public static final double E_3      = 164.814;
	public static final double E_3_FLAT = 155.563;
	public static final double D_3      = 146.832;
	public static final double D_3_FLAT = 138.591;
	public static final double C_3      = 130.813;
	public static final double B_2      = 123.471;
	public static final double B_2_FLAT = 116.541;
	public static final double A_2      = 110.000;
	public static final double A_2_FLAT = 103.826;
	public static final double G_2      = 97.9989;
	public static final double G_2_FLAT = 92.4986;
	public static final double F_2      = 87.3071;
	public static final double E_2      = 82.4069;
	public static final double E_2_FLAT = 77.7817;
	public static final double D_2      = 73.4162;
	public static final double D_2_FLAT = 69.2957;
	public static final double C_2      = 65.4064;
	public static final double B_1      = 61.7354;
	public static final double B_1_FLAT = 58.2705;
	public static final double A_1      = 55.0000;
	public static final double A_1_FLAT = 51.9131;
	public static final double G_1      = 48.9994;
	public static final double G_1_FLAT = 46.2493;
	public static final double F_1      = 43.6535;
	public static final double E_1      = 41.2034;
	public static final double E_1_FLAT = 38.8909;
	public static final double D_1      = 36.7081;
	public static final double D_1_FLAT = 34.6478;
	public static final double C_1      = 32.7032;
	public static final double B_0      = 30.8677;
	public static final double B_0_FLAT = 29.1352;
	public static final double A_0      = 27.5000;
	
	//
	// Sharp/flat synonyms
	//

	public static final double C_8_FLAT  = B_7;
	public static final double C_7_FLAT  = B_6;
	public static final double C_6_FLAT  = B_5;
	public static final double C_5_FLAT  = B_4;
	public static final double C_4_FLAT  = B_3;
	public static final double C_3_FLAT  = B_2;
	public static final double C_2_FLAT  = B_1;
	public static final double C_1_FLAT  = B_0;

	public static final double F_7_FLAT  = E_7;
	public static final double F_6_FLAT  = E_6;
	public static final double F_5_FLAT  = E_5;
	public static final double F_4_FLAT  = E_4;
	public static final double F_3_FLAT  = E_3;
	public static final double F_2_FLAT  = E_2;
	public static final double F_1_FLAT  = E_1;
	
	public static final double B_7_SHARP = C_8;
	public static final double B_6_SHARP = C_7;
	public static final double B_5_SHARP = C_6;
	public static final double B_4_SHARP = C_5;
	public static final double B_3_SHARP = C_4;
	public static final double B_2_SHARP = C_3;
	public static final double B_1_SHARP = C_2;
	public static final double B_0_SHARP = C_1;
	
	public static final double E_7_SHARP = F_7;
	public static final double E_6_SHARP = F_6;
	public static final double E_5_SHARP = F_5;
	public static final double E_4_SHARP = F_4;
	public static final double E_3_SHARP = F_3;
	public static final double E_2_SHARP = F_2;
	public static final double E_1_SHARP = F_1;
	
	public static final double A_7_SHARP = B_7_FLAT;
	public static final double A_6_SHARP = B_6_FLAT;
	public static final double A_5_SHARP = B_5_FLAT;
	public static final double A_4_SHARP = B_4_FLAT;
	public static final double A_3_SHARP = B_3_FLAT;
	public static final double A_2_SHARP = B_2_FLAT;
	public static final double A_1_SHARP = B_1_FLAT;
	public static final double A_0_SHARP = B_0_FLAT;
	
	public static final double G_7_SHARP = A_7_FLAT;
	public static final double G_6_SHARP = A_6_FLAT;
	public static final double G_5_SHARP = A_5_FLAT;
	public static final double G_4_SHARP = A_4_FLAT;
	public static final double G_3_SHARP = A_3_FLAT;
	public static final double G_2_SHARP = A_2_FLAT;
	public static final double G_1_SHARP = A_1_FLAT;
	
	public static final double F_7_SHARP = G_7_FLAT;
	public static final double F_6_SHARP = G_6_FLAT;
	public static final double F_5_SHARP = G_5_FLAT;
	public static final double F_4_SHARP = G_4_FLAT;
	public static final double F_3_SHARP = G_3_FLAT;
	public static final double F_2_SHARP = G_2_FLAT;
	public static final double F_1_SHARP = G_1_FLAT;
	
	public static final double D_7_SHARP = E_7_FLAT;
	public static final double D_6_SHARP = E_6_FLAT;
	public static final double D_5_SHARP = E_5_FLAT;
	public static final double D_4_SHARP = E_4_FLAT;
	public static final double D_3_SHARP = E_3_FLAT;
	public static final double D_2_SHARP = E_2_FLAT;
	public static final double D_1_SHARP = E_1_FLAT;
	
	public static final double C_7_SHARP = D_7_FLAT;
	public static final double C_6_SHARP = D_6_FLAT;
	public static final double C_5_SHARP = D_5_FLAT;
	public static final double C_4_SHARP = D_4_FLAT;
	public static final double C_3_SHARP = D_3_FLAT;
	public static final double C_2_SHARP = D_2_FLAT;
	public static final double C_1_SHARP = D_1_FLAT;
	
	//
	// Common names/synonyms
	//

	// Synonym for A_4
	public static final double _A440 = A_4;
	
	// Synonym for C_8
	public static final double _EIGHTH_OCTAVE_C = C_8;
	// Synonym for C_8
	public static final double _5_LINE_OCTAVE = C_8;
	
	// Synonym for C_7
	public static final double _DOUBLE_HIGH_C = C_7;
	// Synonym for C_7
	public static final double _4_LINE_OCTAVE = C_7;
	
	// Synonym for C_6
	public static final double _SOPRANO_C = C_6;
	// Synonym for C_6
	public static final double _3_LINE_OCTAVE = C_6;
	
	// Synonym for C_5
	public static final double _TENOR_C = C_5;
	// Synonym for C_5
	public static final double _2_LINE_OCTAVE = C_5;
	
	// Synonym for C_4
	public static final double _MIDDLE_C = C_4;
	// Synonym for C_4
	public static final double _1_LINE_OCTAVE = C_4;
	
	// Synonym for C_3
	public static final double _LOW_C = C_3;
	// Synonym for C_3
	public static final double _SMALL_OCTAVE = C_3;
	
	// Synonym for C_2
	public static final double _DEEP_C = C_2;
	// Synonym for C_2
	public static final double _GREAT_OCTAVE = C_2;
	
	// Synonym for C_1
	public static final double _PEDAL_C = C_1;
	// Synonym for C_1
	public static final double _CONTRA_OCTAVE = C_1;
	
	// Synonym for A_0
	public static final double _DOUBLE_PEDAL_A = A_0;
	// Synonym for A_0
	public static final double _SUB_CONTRA_OCTAVE = A_0;

}
