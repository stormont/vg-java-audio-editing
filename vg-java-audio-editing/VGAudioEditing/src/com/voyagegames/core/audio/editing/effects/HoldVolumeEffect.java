package com.voyagegames.core.audio.editing.effects;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public class HoldVolumeEffect extends AbstractEffect {
	
	private final double mLevel;
	
	public HoldVolumeEffect(final double level) {
		mLevel = level;
	}

	@Override
	public IWavSample applyTo(final IWavSample wavSample, final int startSampleIndex, final int endSampleIndex) {
		super.applyTo(wavSample, startSampleIndex, endSampleIndex);
		
		final double[] sample = wavSample.sample();
		final int sampleSize = sample.length;
		
        for (int i = startSampleIndex; i < sampleSize && i < endSampleIndex; ++i) {
        	sample[i] = sample[i] * mLevel;
        }
        
        wavSample.setSample(sample);
        return wavSample;
	}

}
