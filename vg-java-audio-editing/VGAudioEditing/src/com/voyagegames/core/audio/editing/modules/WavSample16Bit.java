package com.voyagegames.core.audio.editing.modules;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;



// Based on: http://marblemice.blogspot.com/2010/04/generate-and-play-tone-in-android.html
// Generates a 16-bit WAV PCM
public class WavSample16Bit extends AbstractWavSample {
	
	public WavSample16Bit(final int sampleRate, final int durationInMilliSeconds, final double toneFrequency, final short numChannels) {
		super(sampleRate, durationInMilliSeconds, toneFrequency, numChannels);
	}

	@Override
	public short format() {
		return WavFile.UNCOMPRESSED_PCM_FORMAT;
	}

	@Override
	public short bitSize() {
		return Short.SIZE;
	}
	
	@Override
	public int maximumAmplitude() {
		return Short.MAX_VALUE;
	}

	@Override
    public IWavSample finalizeOutput() {
        // Convert to 16 bit pcm sound array.
        // Assumes the sample buffer is normalised.
        int idx = 0;
        
        for (final double dVal : mSample) {
            final short val = (short) (dVal * maximumAmplitude());
            
            // In 16 bit wav PCM, first byte is the low order byte
            mGenerated[idx++] = (byte) (val & 0x00ff);
            mGenerated[idx++] = (byte) ((val & 0xff00) >>> 8);
        }
        
        return this;
    }

	@Override
	public void generateSampleFromGenerated() {
		final int generatedLength = mGenerated.length;
		
		for (int idx = 0; idx < generatedLength; idx += 2) {
			final double val = (double)(((short)mGenerated[idx]) + (((short)mGenerated[idx + 1]) << 8));
			mSample[idx / 2] = val / ((double)maximumAmplitude());
		}
	}

}
