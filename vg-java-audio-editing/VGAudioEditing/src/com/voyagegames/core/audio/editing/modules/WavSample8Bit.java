package com.voyagegames.core.audio.editing.modules;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;



// Based on: http://marblemice.blogspot.com/2010/04/generate-and-play-tone-in-android.html
// Generates a 16-bit WAV PCM
public class WavSample8Bit extends AbstractWavSample {
	
	public WavSample8Bit(final int sampleRate, final int durationInMilliSeconds, final double toneFrequency, final short numChannels) {
		super(sampleRate, durationInMilliSeconds, toneFrequency, numChannels);
	}

	@Override
	public short format() {
		return WavFile.UNCOMPRESSED_PCM_FORMAT;
	}

	@Override
	public short bitSize() {
		return Byte.SIZE;
	}
	
	@Override
	public int maximumAmplitude() {
		return Byte.MAX_VALUE;
	}

	@Override
    public IWavSample finalizeOutput() {
        // Convert to 16 bit pcm sound array.
        // Assumes the sample buffer is normalised.
        int idx = 0;
        
        for (final double dVal : mSample) {
            final byte val = (byte) (dVal * maximumAmplitude());
            mGenerated[idx++] = (byte) (val & 0x00ff);
        }
        
        return this;
    }

	@Override
	public void generateSampleFromGenerated() {
		int idx = 0;
		
		for (final byte val : mGenerated) {
			mSample[idx] = ((double)val) / ((double)maximumAmplitude());
		}
	}

}
