package com.voyagegames.core.audio.editing.modules;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public abstract class AbstractWavSample implements IWavSample {

	protected final int mSampleRate;
	protected final int mNumSamples;
	protected final short mNumChannels;
	protected final int mDurationInMilliSeconds;
	protected final double mToneFrequencyHz;
	protected final double[] mSample;
	protected final byte[] mGenerated;
	
	public AbstractWavSample(final int sampleRate, final int durationInMilliSeconds, final double toneFrequency, final short numChannels) {
		this.mSampleRate = sampleRate;
		this.mDurationInMilliSeconds = durationInMilliSeconds;
		this.mToneFrequencyHz = toneFrequency;
		this.mNumChannels = numChannels;
		this.mNumSamples = sampleRate * (mDurationInMilliSeconds / 1000);
		this.mSample = new double[mNumSamples];
		this.mGenerated = new byte[mNumSamples * bitSize() / 8];
	}

	@Override
	public int sampleRate() {
		return mSampleRate;
	}

	@Override
	public int numSamples() {
		return mNumSamples;
	}

	@Override
	public short numChannels() {
		return mNumChannels;
	}

	@Override
	public int durationInMilliSeconds() {
		return mDurationInMilliSeconds;
	}

	@Override
	public double toneFrequencyHz() {
		return mToneFrequencyHz;
	}
	
	@Override
	public double[] sample() {
		return mSample;
	}
	
	@Override
	public IWavSample setSample(final double[] sample) {
		if (sample.length > mSample.length) {
			System.arraycopy(sample, 0, mSample, 0, mSample.length);
		} else {
			System.arraycopy(sample, 0, mSample, 0, sample.length);
		}
		
		return this;
	}

	@Override
    public IWavSample generateSample() {
        for (int i = 0; i < mNumSamples; ++i) {
            mSample[i] = Math.sin(2 * Math.PI * mToneFrequencyHz * i / mSampleRate);
        }
        
        return this;
	}
	
	@Override
	public byte[] generated() {
		return mGenerated;
	}
	
	@Override
	public IWavSample setGenerated(final byte[] generated) {
		if (generated.length != mGenerated.length) {
			throw new IllegalArgumentException("Generated data is an invalid length: " + generated.length + " out of " + mGenerated.length);
		}
		
		System.arraycopy(generated, 0, mGenerated, 0, generated.length);
		return this;
	}

}
