package com.voyagegames.core.audio.editing.modules;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public class WavFile {
	
	public static final int HEADER_SIZE = 44;
	public static final short UNCOMPRESSED_PCM_FORMAT = 1;
	
	/*
     * As defined here: http://www.sonicspot.com/guide/wavefiles.html
	 */
	
	public static byte[] generateHeader(final IWavSample sample) {
		final byte[] data = dataChunk(sample);
		final byte[] format = formatChunk(sample);
		final byte[] header = headerChunk(sample);
		final byte[] result = new byte[header.length + format.length + data.length];

		final int fileSize = data.length + format.length + 4 + sample.generated().length;
		packInt(header, 4, fileSize);
		
		System.arraycopy(header, 0, result, 0, header.length);
		System.arraycopy(format, 0, result, header.length, format.length);
		System.arraycopy(data, 0, result, header.length + format.length, data.length);
		return result;
	}
	
	public static IWavSample generateSample(final byte[] data) {
		if (data.length < (HEADER_SIZE + 1)) {
			throw new IllegalArgumentException("Data has an invalid length");
		}
		
		if (!validateText(data, 0, new byte[] { 'R', 'I', 'F', 'F' })) {
			throw new IllegalArgumentException("Data is not of RIFF format");
		}
		
		final int totalSize = unpackInt(data, 4);
		
		if (totalSize != (data.length - 8)) {
			throw new IllegalArgumentException("Data has an invalid total size: " + totalSize + " rather than " + data.length);
		}
		
		if (!validateText(data, 8, new byte[] { 'W', 'A', 'V', 'E' })) {
			throw new IllegalArgumentException("Data is not of WAVE format");
		}
		
		if (!validateText(data, 12, new byte[] { 'f', 'm', 't', ' ' })) {
			throw new IllegalArgumentException("fmt element not found");
		}

		if (unpackInt(data, 16) != 16) {
			throw new IllegalArgumentException("This class can only process uncompressed PCM data");
		}
		
		final short format = unpackShort(data, 20);
		
		if (format != UNCOMPRESSED_PCM_FORMAT) {
			throw new IllegalArgumentException("This class can only process uncompressed PCM data");
		}
		
		final short numChannels = unpackShort(data, 22);
		final int sampleRate = unpackInt(data, 24);
		final short bitSize = unpackShort(data, 34);
		
		if (bitSize != 8 && bitSize != 16) {
			throw new IllegalArgumentException("This class can only process 8- and 16-bit samples");
		}
		
		boolean foundData = false;
		int offset = 36;
		
		while (offset < data.length) {
			if (validateText(data, offset, new byte[] { 'd', 'a', 't', 'a' })) {
				foundData = true;
				break;
			}
			
			++offset;
		}
		
		if (!foundData) {
			throw new IllegalArgumentException("Could not find data element");
		}
		
		if (offset + 8 >= data.length) {
			throw new IllegalArgumentException("Data element is malformed");
		}
		
		final int dataSize = unpackInt(data, offset + 4);
		final int remainingSize = data.length - offset - 8;
		
		if (dataSize > remainingSize) {
			throw new IllegalArgumentException("Specified data size is too long: " + dataSize + " out of " + remainingSize);
		}
		
		final int numSamples = dataSize / (bitSize / 8);
		final int durationInMilliseconds = (numSamples / sampleRate) * 1000;
		final byte[] sampleData = new byte[dataSize];
		
		System.arraycopy(data, offset + 8, sampleData, 0, dataSize);
		
		IWavSample sample = null;
		
		if (bitSize == 8) {
			sample = new WavSample8Bit(sampleRate, durationInMilliseconds, Frequency.A_4, numChannels);
		} else if (bitSize == 16) {
			sample = new WavSample16Bit(sampleRate, durationInMilliseconds, Frequency.A_4, numChannels);
		} else {
			throw new IllegalArgumentException("Something bad happened; should not have hit this");
		}
		
		sample.setGenerated(sampleData);
		sample.generateSampleFromGenerated();
		return sample;
	}
	
	private static boolean validateText(final byte[] data, final int offset, final byte[] text) {
		if (    data[offset]     == text[0] &&
				data[offset + 1] == text[1] &&
				data[offset + 2] == text[2] &&
				data[offset + 3] == text[3]) {
			return true;
		}
		
		return false;
	}
	
	private static void packShort(final byte[] buffer, final int offset, final short data) {
		buffer[offset]     = (byte) (data & 0x00ff);
		buffer[offset + 1] = (byte) ((data & 0xff00) >>> 8);
	}
	
	private static short unpackShort(final byte[] buffer, final int offset) {
		short result = 0;
		result |= buffer[offset] & 0x00ff;
		result |= (buffer[offset + 1] << 8) & 0x00ff;
		return result;
	}
	
	private static void packInt(final byte[] buffer, final int offset, final int data) {
		buffer[offset]     = (byte) (data & 0x000000ff);
		buffer[offset + 1] = (byte) ((data & 0x0000ff00) >>> 8);
		buffer[offset + 2] = (byte) ((data & 0x00ff0000) >>> 16);
		buffer[offset + 3] = (byte) ((data & 0xff000000) >>> 24);
	}
	
	private static int unpackInt(final byte[] buffer, final int offset) {
		int result = 0;
		result |= buffer[offset] & 0x000000ff;
		result |= (buffer[offset + 1] << 8) & 0x0000ff00;
		result |= (buffer[offset + 2] << 16 & 0x00ff0000);
		result |= (buffer[offset + 3] << 24) & 0xff000000;
		return result;
	}
	
	private static byte[] headerChunk(final IWavSample sample) {
		final byte[] chunk = new byte[12];
		
		chunk[0] = 'R';
		chunk[1] = 'I';
		chunk[2] = 'F';
		chunk[3] = 'F';
		
		// Determine final size later (bits 4-7)
		
		chunk[8]  = 'W';
		chunk[9]  = 'A';
		chunk[10] = 'V';
		chunk[11] = 'E';
		return chunk;
	}
	
	private static byte[] formatChunk(final IWavSample sample) {
		final byte[] chunk = new byte[24];  // No extra data bytes for non-uncompressed PCM data
		
		chunk[0] = 'f';
		chunk[1] = 'm';
		chunk[2] = 't';
		chunk[3] = ' ';
		
		packInt(chunk, 4, 16);  // + extra data bytes for non-uncompressed PCM data
		packShort(chunk, 8, sample.format());

		final short numChannels = sample.numChannels();
		packShort(chunk, 10, numChannels);
		
		final int sampleRate = sample.sampleRate();
		packInt(chunk, 12, sampleRate);
		
		final short bitSize = sample.bitSize();
		final short bytesPerChannel = (short)((bitSize * numChannels) / 8);
		packInt(chunk, 16, sampleRate * bytesPerChannel);
		
		packShort(chunk, 20, bytesPerChannel);
		packShort(chunk, 22, bitSize);
		return chunk;
	} 
	
	private static byte[] dataChunk(final IWavSample sample) {
		final byte[] chunk = new byte[8];
		
		chunk[0] = 'd';
		chunk[1] = 'a';
		chunk[2] = 't';
		chunk[3] = 'a';
		
		packInt(chunk, 4, sample.generated().length);
		return chunk;
	}

}
