package com.voyagegames.core.audio.editing.effects;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public class ReverberationEffect extends AbstractEffect {
	
	private final double mAbsorption;
	
	public ReverberationEffect(final double absorption) {
		mAbsorption = 1.0 - absorption;
	}

	@Override
	public IWavSample applyTo(final IWavSample wavSample, final int startSampleIndex, final int endSampleIndex) {
		super.applyTo(wavSample, startSampleIndex, endSampleIndex);
		
		final double[] sample = wavSample.sample();
		final int lastIndex = endSampleIndex < sample.length ? endSampleIndex : sample.length;
		final int range = lastIndex - startSampleIndex;
		
		int delay = 10;
		double[] sound = new double[range];
		
		System.arraycopy(sample, startSampleIndex, sound, 0, sound.length);
		
		for (int i = 0; i < 10 && delay < range; i++) {
			sound = reverb(sound, delay);
			delay = delay * 2;
		}
		
		for (int i = 0; i < range; i++) {
			sample[i + startSampleIndex] = sound[i];
		}
        
        wavSample.setSample(sample);
        return wavSample;
	}
	
	private double[] reverb(final double[] sound, final int delay) {
		final double[] result = new double[sound.length];
		
		for (int i = 0; i < delay; i++) {
			result[i] = mAbsorption * sound[i];
		}
		
		for (int i = delay; i < sound.length; i++) {
			result[i] = (mAbsorption * sound[i]) + sound[i - delay] - (mAbsorption * result[i - delay]);
		}
		
		return result;
	}

}
