package com.voyagegames.core.audio.editing.effects;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public class VibratoEffect extends AbstractEffect {
	
	private final double mNumModulations;
	private final double mVibratoRange;
	
	public VibratoEffect(final int numModulations, final double vibratoRange) {
		mNumModulations = numModulations;
		mVibratoRange = vibratoRange;
	}

	@Override
	public IWavSample applyTo(final IWavSample wavSample, final int startSampleIndex, final int endSampleIndex) {
		super.applyTo(wavSample, startSampleIndex, endSampleIndex);
		
		final double[] sample = wavSample.sample();
		final int sampleSize = sample.length;
		final int modulationPeriod = (int) (mRange / mNumModulations);

        for (int i = startSampleIndex; i < sampleSize && i < endSampleIndex; ++i) {
        	final double perc = ((double)((i % modulationPeriod) + 1)) / ((double)modulationPeriod);
        	sample[i] = sample[i] * (1.0 - (perc * mVibratoRange));
        	
        	if (Math.abs(sample[i]) > 1.0) {
        		throw new IllegalArgumentException("Sample not normalized at " + i + ": " + sample[i]);
        	}
        }
        
        wavSample.setSample(sample);
        return wavSample;
	}

}
