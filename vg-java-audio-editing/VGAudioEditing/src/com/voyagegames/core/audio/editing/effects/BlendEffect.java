package com.voyagegames.core.audio.editing.effects;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public class BlendEffect extends AbstractEffect {
	
	private final IWavSample mAltSample;
	private final int mAltFullWeightIndex;
	
	public BlendEffect(final IWavSample altSample, final int altFullWeightIndex) {
		mAltSample = altSample;
		mAltFullWeightIndex = altFullWeightIndex;
	}

	@Override
	public IWavSample applyTo(final IWavSample wavSample, final int startSampleIndex, final int endSampleIndex) {
		super.applyTo(wavSample, startSampleIndex, endSampleIndex);
		
		final double[] sample = wavSample.sample();
		final double[] altSample = mAltSample.sample();
		final int sampleSize = altSample.length > sample.length ? sample.length : altSample.length;
		final int range = mAltFullWeightIndex - startSampleIndex;
		
        for (int i = startSampleIndex; i < sampleSize && i < mAltFullWeightIndex; ++i) {
        	final double perc = (double)(i - startSampleIndex + 1) / range;
        	final double startWeight = (1.0 - perc) * sample[i];
        	final double endWeight = perc * altSample[i];
        	
        	sample[i] = startWeight + endWeight;
        }
        
        for (int i = mAltFullWeightIndex; i < sampleSize && i < endSampleIndex; ++i) {
        	sample[i] = altSample[i];
        }
        
        wavSample.setSample(sample);
        return wavSample;
	}

}
