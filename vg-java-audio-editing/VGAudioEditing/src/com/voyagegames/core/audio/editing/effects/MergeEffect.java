package com.voyagegames.core.audio.editing.effects;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public class MergeEffect extends AbstractEffect {
	
	private final IWavSample mAltSample;
	private final double mAltWeight;
	
	public MergeEffect(final IWavSample altSample, final double altWeight) {
		mAltSample = altSample;
		mAltWeight = altWeight;
	}

	@Override
	public IWavSample applyTo(final IWavSample wavSample, final int startSampleIndex, final int endSampleIndex) {
		super.applyTo(wavSample, startSampleIndex, endSampleIndex);
		
		final double[] sample = wavSample.sample();
		final double[] altSample = mAltSample.sample();
		final int sampleSize = altSample.length > sample.length ? sample.length : altSample.length;
		
        for (int i = startSampleIndex; i < sampleSize && i < endSampleIndex; ++i) {
        	sample[i] = ((1.0 - mAltWeight) * sample[i]) + (mAltWeight * altSample[i]);
        }
        
        wavSample.setSample(sample);
        return wavSample;
	}

}
