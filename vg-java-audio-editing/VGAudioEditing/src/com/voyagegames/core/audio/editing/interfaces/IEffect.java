package com.voyagegames.core.audio.editing.interfaces;

public interface IEffect {
	
	IWavSample apply(IWavSample wavSample);
	IWavSample applyTo(IWavSample wavSample, int startSampleIndex, int endSampleIndex);

}
