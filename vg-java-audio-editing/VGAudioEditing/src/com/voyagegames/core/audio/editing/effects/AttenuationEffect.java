package com.voyagegames.core.audio.editing.effects;

import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public class AttenuationEffect extends AbstractEffect {
	
	private final double mStart;
	private final double mEnd;
	
	public AttenuationEffect(final double start, final double end) {
		mStart = start;
		mEnd = end;
	}

	@Override
	public IWavSample applyTo(final IWavSample wavSample, final int startSampleIndex, final int endSampleIndex) {
		super.applyTo(wavSample, startSampleIndex, endSampleIndex);
		
		final double[] sample = wavSample.sample();
		final int sampleSize = sample.length;
		
        for (int i = startSampleIndex; i < sampleSize && i < endSampleIndex; ++i) {
        	final double perc = (double)(i - startSampleIndex + 1) / mRange;
        	final double startWeight = (1.0 - perc) * mStart;
        	final double endWeight = perc * mEnd;
        	
        	sample[i] = sample[i] * (startWeight + endWeight);
        }
        
        wavSample.setSample(sample);
        return wavSample;
	}

}
