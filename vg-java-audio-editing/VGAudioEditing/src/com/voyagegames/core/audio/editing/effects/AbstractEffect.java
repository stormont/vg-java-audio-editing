package com.voyagegames.core.audio.editing.effects;

import com.voyagegames.core.audio.editing.interfaces.IEffect;
import com.voyagegames.core.audio.editing.interfaces.IWavSample;

public abstract class AbstractEffect implements IEffect {

	protected double mRange;
	
	@Override
	public IWavSample apply(final IWavSample wavSample) {
		return applyTo(wavSample, 0, wavSample.sample().length);
	}

	@Override
	public IWavSample applyTo(final IWavSample wavSample, final int startSampleIndex, final int endSampleIndex) {
		mRange = endSampleIndex - startSampleIndex;
		return wavSample;
	}

}
